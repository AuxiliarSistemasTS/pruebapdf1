<?php
require('../fpdf.php');

class PDF extends FPDF
{
	// Cabecera de p�gina
	function Header()
	{
		$this->SetFont('Arial','B',14);
		$this->Cell(85);
	  	$this->Cell(20,15,'CHECK LIST - VENDEDORES DE PLAYA / MINIMERCADO',0,1,'C');
		$this->Ln(1);

		$this->SetFont('Arial','',11);
		$this->Cell(7);
		$this->Cell(0,0,'CANDIDATO(A): ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,'NOMBRE DEL CANDIDATO',0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'PUESTO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,'CARGO DEL POSTULANTE',0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'ESTACION DE SERVICIO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,'ESTACI�N',0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FECHA DE EXAMEN MEDICO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,'00/00/0000',0,1,'L');

		$this->Ln(7);
		$this->Cell(7);
		$this->Cell(0,0,'FECHA DE INGRESO: ',0,1,'L');
		$this->Cell(80);
		$this->Cell(0,0,'00/00/0000',0,1,'L');

		$this->Ln(5);
		$this->SetFont('Arial','B',11);
		$this->SetFillColor(230,230,230);
		$this->Cell(4);
		$this->Cell(0,6,"DOCUMENTOS DE LA CONSULTORA ",0,1,'C',true);
		$this->Ln(5);
	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',11);
	
	$pdf->Cell(7);
	$pdf->Cell(0,0,'01 COPIA DE DNI VIGENTE',0,1,'L');
	$pdf->Rect(180, 66, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'01 COPIA DE HIJOS (-18 a�os) Y/O CONYUGUE (Partida de Matrimonio)',0,1,'L');
	$pdf->Rect(180, 73, 20, 7, '');
	
	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'01 COPIA DE CARNE DE SANIDAD',0,1,'L');
	$pdf->Rect(180, 80, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'FICHA DE DATOS PERSONALES',0,1,'L');
	$pdf->Rect(180, 87, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'ORIGINAL DE CERTIFICADO DE ANTECEDENTES POLICIALES VIGENTE',0,1,'L');
	$pdf->Rect(180, 94, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'01 COPIA DE SERVICIOS P�BLICOS (Luz, Agua o Tel�fono)',0,1,'L');
	$pdf->Rect(180, 101, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N JURADA DE NO REINGRESO',0,1,'L');
	$pdf->Rect(180, 108, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'CV POSTULANTE',0,1,'L');
	$pdf->Rect(180, 115, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'COPIA DE CERTIFICADO DE ESTUDIOS / DECLARACION JURADA DE ESTUDIOS',0,1,'L');
	$pdf->Rect(180, 122, 20, 7, '');

	$pdf->Ln(6);
	$pdf->SetFont('Arial','B',11);
	$pdf->SetFillColor(230,230,230);
	$pdf->Cell(4);
	$pdf->Cell(0,6,"DOCUMENTOS DE BIENESTAR SOCIAL",0,1,'C',true);
	$pdf->Ln(6);

	$pdf->SetFont('Arial','',11);

	$pdf->Cell(7);
	$pdf->Cell(0,0,'INSCRIPCI�N DE DERECHOHABIENTES EN T-REGISTRO',0,1,'L');
	$pdf->Rect(180, 140, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N JURADA DE VERACIDAD',0,1,'L');
	$pdf->Rect(180, 147, 20,7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N JURADA DE DOMICILIO',0,1,'L');
	$pdf->Rect(180, 154, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N JURADA DEL TRABAJADOR-RENTAS DE 5TA. Y 47A. CATEGOR�A',0,1,'L');
	$pdf->Rect(180, 161, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N JURADA DE SISTEMA DE PENSIONES',0,1,'L');
	$pdf->Rect(180, 168, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'FICHA DE ELECCI�N DEL SISTEMA PENSIONARIO',0,1,'L');
	$pdf->Rect(180, 175, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'CONFORMIDAD DE RECEPCI�N Y CUMPLIMIENTO DE USO DE EPP',0,1,'L');
	$pdf->Rect(180, 182, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'AUTORIZACI�N DE EX�MENES M�DICOS PRE-OCUPACIONALES',0,1,'L');
	$pdf->Rect(180, 189, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'IDENTIFICACI�N DE RIESGOS Y RECOMENDACIONES DE SST',0,1,'L');
	$pdf->Rect(180, 196, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'DECLARACI�N DE CONSENTIMIENTO PARA TRATAMIENTO DE DATOS PERSONALES',0,1,'L');
	$pdf->Rect(180, 203, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'FICHA DE BIENESTAR SOCIAL',0,1,'L');
	$pdf->Rect(180, 210, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'ACUERDO DE ENVIO DE BOLETA ELECTRONICA',0,1,'L');
	$pdf->Rect(180, 217, 20, 7, '');

	$pdf->Ln(8);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'PAGO DE UTILIDADES - AUTORIZACI�N',0,1,'L');
	$pdf->Rect(180, 224, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'MOF POR PUESTO',0,1,'L');
	$pdf->Rect(180, 231, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'CARGOS DE RECEPCI�N DE REGLAMENTO INTERNO DE TRABAJO',0,1,'L');
	$pdf->Rect(180, 238, 20, 7, '');

	$pdf->Ln(8);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'CARGOS DE RECEPCI�N DE REGLAMENTO INTERNO DE SST',0,1,'L');
	$pdf->Rect(180, 245, 20, 7, '');

	$pdf->Ln(7);
	$pdf->Cell(7);
	$pdf->Cell(0,0,'CARGOS DE RECEPCI�N DE BOLET�N INFORMATIVO DEL SISTEMA DE PENSIONES',0,1,'L');
	$pdf->Rect(180, 252, 20, 7, '');

	$pdf->Output();
?>
