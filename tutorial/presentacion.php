<?php
require('../fpdf.php');

class PDF extends FPDF
{
	// Cabecera de p�gina
	function Header()
	{
		$this->Rect(5, 10, 60, 15, '');
		$this->Rect(65, 10, 92, 15, '');
		$this->Rect(157, 10, 45, 15, '');
		$this->Image('logo.png',7,12,60);
		$this->Image('logo_primax.png',158,12,42);

		$this->SetFont('Arial','',14);
		$this->Cell(12);
	  	$this->Cell(0,17,'FORMATO DE ENVIO A ENTREVISTA',0,1,'C');

	  	$this->Ln(3);
	  	$this->Rect(5, 25, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 25, 155, 10, 'DF');
	  	$this->SetFont('Arial','',12);
		$this->Cell(-3);
	  	$this->Cell(0,0,'Postulante',0,1,'L');

	  	$this->Ln(10);
	  	$this->Rect(5, 35, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 35, 46, 10, 'DF');
		$this->Cell(-3);
	  	$this->Cell(0,0,'Fecha',0,1,'L');

	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(138, 35, 64, 10, 'DF');
		$this->Cell(85);
	  	$this->Cell(0,0,'Cargo al que postula',0,1,'L');

	  	$this->Ln(10);
	  	$this->Rect(5, 45, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 45, 46, 10, 'DF');
		$this->Cell(-3);
	  	$this->Cell(0,0,'Entrevistador',0,1,'L');

	  	$this->Rect(93, 45, 46, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(138, 45, 64, 10, 'DF');
		$this->Cell(85);
	  	$this->Cell(0,0,'Hora',0,1,'L');

	  	$this->Ln(10);
	  	$this->Rect(5, 55, 42, 10, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 55, 155, 10, 'DF');
		$this->Cell(-3);
	  	$this->Cell(0,0,'Lugar de entrevista',0,1,'L');

	  	$this->Ln(12);
	  	$this->Rect(5, 65, 42, 15, '');
	  	$this->SetFillColor(230,230,230);
	  	$this->Rect(47, 65, 155, 15, 'DF');
		$this->Cell(-3);
	  	$this->Cell(0,0,'Observaciones',0,1,'L');

	}
}

	// Creaci�n del objeto de la clase heredada
	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->SetFont('Arial','',11);

	$pdf->Output();
?>
