<?php
require('../fpdf.php');

class PDF extends FPDF
{
	// Cabecera de p�gina
	function carta($fecha, $estacion, $trabajador, $cargo)
	{
		//-------logo y fecha alta
		// Logo de la empresa
		$this->Image('logo.png',10,15,80);
		// Arial bold 15
		$this->SetFont('Arial','',12);
		// Movernos a la derecha
		$this->Cell(80);
		// FECHA DE alta
		$this->Cell(150,30,$fecha,0,0,'C');
		// Salto de l�nea
		$this->Ln(25);


		//titulo general
		// Arial bold 15
		$this->SetFont('Arial','b',22);
		// Movernos a la derecha
		$this->Cell(85);
		// FECHA DE alta
		 $this->Cell(20,30,'CARTA DE PRESENTACI�N',0,1,'C');
	
		// Salto de l�nea
		
		$this->Line(20, 62, 210-20, 62); // 20mm from each edge
		$this->Line(50, 62, 210-50, 62); // 50mm from each edge
	  
		$this->SetDrawColor(188,188,188);
		$this->Line(20,63,210-20,63);
		$this->Ln(13);

		// estacioon
		$this->SetFont('Arial','B',14);
		$this->Cell(9);
		$this->Cell(0,0,$estacion,0,1,'L');
		$this->Ln(13);

		//TEXTO
		$this->Cell(9);
		$this->SetFont('Arial','',14);

		$this->Cell(0,0,'T-Soluciona tiene el agrado de presentar al Sr.:',0,1,'L');

		//NOMBRE DEL POSTULANTE
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$trabajador,0,1,'C');

		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Quien ha ido evaluado a trav�s de un proceso de filtros, para posici�n de:',0,1,'L');

		//CARGO
		$this->Ln(13);
		$this->Cell(80);
		$this->SetFont('Arial','B',14);
		$this->Cell(20,5,$cargo,0,1,'C');

		//direccion y estacion
		$this->Ln(13);
		$this->Cell(9);
		$this->SetFont('Arial','',14);
		$this->MultiCell(151,5,'Direcci�n: Av. Canada Cra.11 con Victor Alzamora - LA VICTORIA / E/S CANADA',0,'C',false);

		//TEXTO
		$this->Ln(13);
		$this->Cell(9);
		$this->MultiCell(170,5,'De esta forma, la persona en cuesti�n culmin� satisfactoriament el paso por nuestra consultora y es considerada APTA.',0,'J',false);
		$this->Ln(5);
		$this->Cell(9);
		$this->Cell(0,0,'Sin otro particular, agradecemos su preferencia.',0,1,'L');
		$this->Ln(10);
		$this->Cell(9);
		$this->Cell(0,0,'Atte.',0,1,'L');

		//FIRMA 
		$this->Ln(10);
		$this->Image('FIRMA CARMEN.png',72,217,75);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(0.3);
		$this->Line(63,255,210-63,255);
		$this->Ln(49);
		$this->Cell(54);
		$this->SetFont('Arial','B',14);
		$this->Cell(0,0,'CARMEN PALACIOS CHAFALOTE',0,1,'L');
		$this->Ln(5);
		$this->Cell(56);
		$this->SetFont('Arial','',14);
		$this->Cell(0,0,'Selecci�n de Personal T-Soluciona','C',1);
	}
}

// Creaci�n del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$contador=3;
$fecha = 'Lima, 10 de setiembre del 2018';
$estacion = 'E/S TAVIRSA:';
$trabajador = 'LA TORRE GUERRERO, JORGE LUIS';
$cargo ='VENDEDOR PLAYA';

for ($i=1; $i <= $contador; $i++) { 
	$pdf->AddPage();
	$pdf -> carta($fecha, $estacion, $trabajador, $cargo);
}
$pdf->Output();
?>
